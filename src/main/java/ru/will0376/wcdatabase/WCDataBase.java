package ru.will0376.wcdatabase;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.utils.Module;
import ru.will0376.windcheckbridge.utils.SubCommands;
import ru.will0376.windcheckbridge.utils.Token;

@Mod(modid = WCDataBase.MOD_ID,
		name = WCDataBase.MOD_NAME,
		version = WCDataBase.VERSION,
		acceptedMinecraftVersions = "[1.12.2]",
		dependencies = "after:windcheckbridge@[3.0.17,)",
		acceptableRemoteVersions = "*")
public class WCDataBase {

	public static final String MOD_ID = "wcdatabase";
	public static final String MOD_NAME = "WCDataBase";
	public static final String VERSION = "@version@";

	@Mod.Instance(MOD_ID)
	public static WCDataBase INSTANCE;
	public static Module module = Module.builder().modid(MOD_ID).setDefaultNames(MOD_NAME).version(VERSION).build();
	public static Token.Requester requester;
	public static SubCommands subCommand;

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		requester = WindCheckBridge.aVersion.registerModule(module);
		subCommand = WindCheckBridge.aVersion.registerNewCommand(new DBCommand());
		if (Configs.configured)
			DataBase.INSTANCE.checkTables();
	}
}
