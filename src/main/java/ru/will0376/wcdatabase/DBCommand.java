package ru.will0376.wcdatabase;

import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import org.apache.commons.cli.CommandLine;
import ru.will0376.windcheckbridge.WindCheckBridge;
import ru.will0376.windcheckbridge.utils.AbstractCommand;

import java.util.Arrays;
import java.util.List;

public class DBCommand extends AbstractCommand {
	public DBCommand() {
		super("database");
	}

	@Override
	public List<Argument> getArgList() {
		return Arrays.asList(getArgBuilder("reloadDB").build(), getArgBuilder("toggleConfigured").build());
	}

	@Override
	public ActionStructure execute(MinecraftServer server, ICommandSender sender, String[] args) throws Exception {
		ActionStructure build = getStartedAction().adminNick(sender.getName()).args(args).build();

		if (!WindCheckBridge.aVersion.senderCanUseCommand(sender, getPermission())) {
			return build.setCanceled("no rights");
		}

		CommandLine line = getLine(args);

		if (line.hasOption("reloadDB")) {
			DataBase.INSTANCE.checkTables();
		} else if (line.hasOption("toggleConfigured")) {
			Configs.configured = !Configs.configured;
		}

		return build;
	}
}
