package ru.will0376.wcdatabase;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = WCDataBase.MOD_ID)
@Mod.EventBusSubscriber
public class Configs {

	public static String host = "jdbc:mysql://127.0.0.1:3306/test?serverTimezone=UTC";
	public static String user = "root";
	public static String pass = "pass";
	public static String tableName = "WindCheckDBLogs";
	public static String columnURL = "URL";
	public static String columnPlayer = "Player";
	public static String columnAdmin = "Admin";
	public static String columnTime = "Time";
	public static boolean configured = false;

	@SubscribeEvent
	public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
		if (event.getModID().equals(WCDataBase.MOD_ID)) {
			ConfigManager.sync(WCDataBase.MOD_ID, Config.Type.INSTANCE);
		}
	}
}
