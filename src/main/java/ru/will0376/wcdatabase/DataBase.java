package ru.will0376.wcdatabase;

import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Log4j2
public enum DataBase {
	INSTANCE;

	public Connection connect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			log.debug("Driver found!");
			Connection dbConnection = DriverManager.getConnection(Configs.host, Configs.user, Configs.pass);
			log.info("Connection to {} successfully", Configs.host);
			return dbConnection;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void checkTables() {
		try {
			Connection c = connect();
			c.prepareStatement(("CREATE TABLE IF NOT EXISTS %logtable%(id int auto_increment primary key, %url% text, %player% " +
					                    "text, %admin% text, %time% text)").replace("%logtable%", Configs.tableName)
					.replace("%url%", Configs.columnURL)
					.replace("%player%", Configs.columnPlayer)
					.replace("%admin%", Configs.columnAdmin)
					.replace("%time%", Configs.columnTime)).execute();
		} catch (Exception e) {
			log.error("Error with database: ", e);
		}
	}

	public void addScreenToLog(String nick, String admin, String url, String time) {
		try {
			log.debug(String.format("AddToLog: %s %s %s %s", nick, admin, url, time));
			connect().prepareStatement(("Insert into %logtable% (%urltable%, %playertable%, %admintable%, %timetable%)" + " " +
					                            "VALUES(\"%url%\", \"%player%\", \"%admin%\", \"%time%\")").replace("%logtable%"
							, Configs.tableName)
					.replace("%url%", url)
					.replace("%player%", nick)
					.replace("%admin%", admin)
					.replace("%time%", time)
					.replace("%urltable%", Configs.columnURL)
					.replace("%playertable%", Configs.columnPlayer)
					.replace("%admintable%", Configs.columnAdmin)
					.replace("%timetable%", Configs.columnTime)).executeUpdate();
		} catch (Exception e) {
			log.error("Error with database: ", e);
		}
	}

}
