package ru.will0376.wcdatabase;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.will0376.windcheckbridge.events.CheckAndPrintEvent;
import ru.will0376.windcheckbridge.utils.Token;

@Mod.EventBusSubscriber
public class Events {

	@SubscribeEvent
	public static void event(CheckAndPrintEvent event) {
		if (event.getText().contains("screen")) {
			Token token = event.getToken();
			DataBase.INSTANCE.addScreenToLog(token.getPlayerNick(), token.getAdminNick(), event.getClickText(),
					String.valueOf(System.currentTimeMillis()));
		}
	}
}
